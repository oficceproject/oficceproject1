let addText= document.querySelector('.addText');
let addListItem = document.querySelector('.addListItem');
let addFile = document.getElementById('addContent');
let arr = [];
let i = 0;
let count;

let main_field = document.querySelector('.main_field');

addText.addEventListener('click', function(){
    count = i++;
    arr.push({
        'el' : 'addTextInHTML_text',
        'index': count,
        "text":'Текст какой-то блаблабла',
      'font': '16',
      'color': 'blue',
      'align': 'left'
} );
  let div = document.createElement('div');
  let span_delete = document.createElement('span');
  div.className = 'text';
  div.textContent = 'Текст какой-то блаблабла';
  span_delete.className ='button_delete';
  span_delete.innerHTML = 'X';
  span_delete.setAttribute('index', count);
  div.appendChild(span_delete);
  main_field.appendChild(div);

  span_delete.addEventListener('click',function(){
   let index = this.getAttribute('index');
    arr.splice(index,1);
   main_field.removeChild(div);
  })
})
addListItem.addEventListener('click', function(){
    count = i++;
    arr.push({
        'el' : 'addListItem',
        'index': count,
        "text":'Текст листа',
      'font': '16',
      'color': 'red',
      'align': 'left',
      'listType': 'TYPE_ALPHANUM' 
    }
    );
    let li = document.createElement('li');
    let span_delete = document.createElement('span');
    li.className = 'textList';
    li.textContent = 'Текст листа';
    span_delete.className ='button_delete';
    span_delete.innerHTML = 'X';
    span_delete.setAttribute('index', count);
    li.appendChild(span_delete);
    main_field.appendChild(li);
    span_delete.addEventListener('click',function(){
     let index = this.getAttribute('index');
      arr.splice(index,1);
     main_field.removeChild(li);
    })
})
addFile.addEventListener('click', function(){
    let promise = fetch('loadendjs.php?data=' + JSON.stringify(arr));
    promise.then(response => {
        return response.text();
    })
    .then(data => {
        textend.innerHTML = data;
    })
});

// window.onbeforeunload = function() {
//     return "Есть несохранённые изменения. Всё равно уходим?";
//   };

  main_field.addEventListener('click', function(e){
    let elems = main_field.children;
    for(let el of elems){
        if(el.classList.contains('active')){
            el.classList.remove('active');
        }
    }

      let text;
      if(e.target.classList.contains('text')){
        text = e.target;
        text.classList.add('active');
      }

      
      
    // console.log(text);
  });

  //buttons
  let left_align = document.querySelector('.left-align');
  let center_align = document.querySelector('.center-align');
  let right_align = document.querySelector('.right-align');


  let alig;
  left_align.onclick = function(){
     alig = 'left';
    AlignItems(alig);
  }
  center_align.onclick = function(){
     alig = 'center';
      AlignItems(alig);
  }
  right_align.onclick = function(){
      alig = 'rigth';
      AlignItems(alig);
  }


  function AlignItems(al){
    let elems = main_field.children;
    let attr;
    for(let el of elems){
        // el.classList.remove('left');
        // el.classList.remove('center');
        // el.classList.remove('rigth');
        if(el.classList.contains('active')){
             el.classList.remove('left');
            el.classList.remove('center');
            el.classList.remove('rigth');
            let nextel = el.firstElementChild;
            attr = nextel.getAttribute('index');
            el.classList.add(al);
        }
    }
    for(let e of arr){
        if(e.index == attr){
            e.align= al;
        }
    }
  }
