<?php
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Converter;
require 'vendor/autoload.php';

$source = "blabla.docx";

// $phpWord = \PhpOffice\PhpWord\IOFactory::createReader('Word2007');

// $phpWord = $objReader->load($source); 

// $phpWord->setDefaultFontSize(24);

$phpWord = \PhpOffice\PhpWord\IOFactory::load($source);;

$phpWord->setDefaultParagraphStyle(
    array(
        'align'      => 'both',
        'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(12),
        'spacing'    => 120,
        )
    );

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
$objWriter->save('doooo.docx');
?>