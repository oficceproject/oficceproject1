<?php

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Converter;
require 'vendor/autoload.php';

    $phpWord = new \PhpOffice\PhpWord\PhpWord(); 

$phpWord->setDefaultFontSize(14);


$properties = $phpWord->getDocInfo();

// $properties->setCreator('Name');
// $properties->setCompany('Company');
// $properties->setTitle('Title');
// $properties->setDescription('Description');
// $properties->setCategory('My category');
// $properties->setLastModifiedBy('My name');
// $properties->setCreated(mktime(0, 0, 0, 3, 12, 2015));
// $properties->setModified(mktime(0, 0, 0, 3, 14, 2015));
// $properties->setSubject('My subject');
// $properties->setKeywords('my, key, word');  

   $sectionStyle = array(
    'orientation' => 'landscape',
    'marginTop' => \PhpOffice\PhpWord\Shared\Converter::pixelToTwip(10),
    // 'marginLeft' => 600,
    // 'marginRight' => 600,
    'colsNum' => 1,
    'pageNumberingStart' => 1,
    // 'borderBottomSize'=>100,
    // 'borderBottomColor'=>'C0C0C0'
    );
   $section = $phpWord->addSection($sectionStyle);

$text = "PHPWord is a library written in pure PHP that provides a set of classes to write to and read from different document file formats.";
$fontStyle = array('name'=>'Nautilus Pompilius', 'size'=>16, 'color'=>'075776', 'bold'=>TRUE, 'italic'=>TRUE);
$parStyle = array('align'=>'right','spaceBefore'=>10);
$section->addText(htmlspecialchars($text), $fontStyle,$parStyle);

 $el =1;
$section->addImage('i4s6rEB_Njc.jpg', ['width'=>100, 'height'=> 100] );

//лист
$fontS = array('name'=>'Nautilus Pompilius', 'size'=>11, 'color'=>'075776','italic'=>TRUE);
$listStyle = array('listType'=>\PhpOffice\PhpWord\Style\ListItem::TYPE_SQUARE_FILLED);

$section->addListItem('Элемент 1', 0, $fontS);
$section->addListItem('Элемент 2', 0, $fontS);
$section->addListItem('Элемент 3', 0, $fontS);
$section->addListItem('Элемент 4', 0, $fontS);
$section->addListItem('Элемент 5', 0, $fontS);
//лист



$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
$objWriter->save('doc.docx');



// header("Content-Description: File Transfer");
// header('Content-Disposition: attachment; filename="first.docx"');
// header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
// header('Content-Transfer-Encoding: binary');
// header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
// header('Expires: 0');
 
// $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
// $objWriter->save("php://output");

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Тайтл страницы</title>
    </head>
    <body>
        <div id="result"></div>
        <input type="submit" id="button">

        <a href="main.php">main.php</a>
        <a href="newtestfile.php">newtestfile.php</a>
        <a href="loadjs.php">loadjs.php</a>
    </body>
     
</html>
